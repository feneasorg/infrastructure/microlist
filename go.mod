module git.feneas.org/feneas/infrastructure/microlist

go 1.13

replace gopkg.in/asn1-ber.v1 => github.com/go-asn1-ber/asn1-ber v1.4.1

require (
	github.com/rs/zerolog v1.17.2
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-00010101000000-000000000000 // indirect
	gopkg.in/ini.v1 v1.52.0
	gopkg.in/ldap.v2 v2.5.1
)
