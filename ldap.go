package main
//
// Microlist - A simple mailinglist for postfix with ldap backend
// Copyright (C) 2020  Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
import (
  "strings"
  "crypto/tls"
  "fmt"

  ldap "gopkg.in/ldap.v2"
)

func fetchSubscribers(listId string) []string {
  return fetchAttribute(fmt.Sprintf("(|(cn=%s)(mail=%s))", listId, listId), "mailalias")
}

func fetchAttribute(filter, attr string) []string {
  res := fetchAttributes(filter, []string{attr})
  for _, entry := range res {
    for _, attrs := range entry.Attributes {
      if strings.EqualFold(attrs.Name, attr) {
        return attrs.Values
      }
    }
  }
  return []string{}
}

// specify a filter and fetch attributes from LDAP server
func fetchAttributes(filter string, attrs []string) []*ldap.Entry {
  var dial *ldap.Conn
  if len(config.LDAPHost) > 8 && config.LDAPHost[0:8] == "ldaps://" {
    conn, err := ldap.DialTLS("tcp", config.LDAPHost[8:],
      &tls.Config{InsecureSkipVerify: true})
    if err != nil {
      logger.Fatal().Err(err).Msg("cannot connect to ldap host")
    }
    dial = conn
  } else if len(config.LDAPHost) > 7 && config.LDAPHost[0:7] == "ldap://" {
    conn, err := ldap.Dial("tcp", config.LDAPHost[7:])
    if err != nil {
      logger.Fatal().Err(err).Msg("cannot connect to ldap host")
    }
    dial = conn
  } else {
    logger.Fatal().Msg("wrong ldap host format (expected: ldap(s)://host:port)")
  }
  defer dial.Close()

  searchRequest := ldap.NewSearchRequest(
    config.LDAPBase, ldap.ScopeWholeSubtree, ldap.NeverDerefAliases,
    0, 0, false, filter, attrs, nil,
  )

  err := dial.Bind(config.LDAPBindDN, config.LDAPBindPW)
  if err != nil {
    logger.Fatal().Err(err).Msg("cannot bind to ldap host")
  }

  search, err := dial.Search(searchRequest)
  if err != nil {
    logger.Fatal().Err(err).Msg("failed to search for ldap entry")
  }

  logger.Debug().
    Str("filter", filter).
    Interface("attributes", attrs).
    Interface("results", search.Entries).
    Msg("attributes fetched")

  return search.Entries
}
