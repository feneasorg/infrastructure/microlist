package main
//
// Microlist - A simple mailinglist for postfix with ldap backend
// Copyright (C) 2020  Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
import (
  "bytes"
  "strings"
  "net/mail"
  "net/smtp"
  "errors"
  "fmt"
)

func handleReceivedMessage(msg *mail.Message) {
  from, err := mail.ParseAddress(msg.Header.Get("From"))
  if err != nil {
    logger.Error().Err(err).Msg("cannot parse From address")
    return
  }

  // NOTE if for some reason the sender is the same as our
  // address space abort cause this will end in an infinity loop
  if strings.Contains(from.Address, fmt.Sprintf(config.PrefixAddress, "")) {
    logger.Error().Msgf("the address space '%s' is not allowed! Skipping ..",
      fmt.Sprintf(config.PrefixAddress, ""))
    return
  }

  // pre postmaster filtering
  content := strings.ToLower(msg.Header.Get("Content-Type"))
  if strings.HasPrefix(content, "message/") ||
  strings.HasPrefix(content, "multipart/report") {
    logger.Error().Msg("cannot handle message or multipart reports")
    return
  }

  lists := lookupLists(msg)
  if len(lists) == 0 {
    handleErrorResponse(from, msg)
    logger.Debug().Msg("no registered mailing list found")
    return
  }

  var errs []error
  for _, list := range lists {
    // check if the mail address is allowed to post messages to this list
    if !list.CanPost(from) {
      logger.Info().Msgf("%s is not allowed to post to %s", from.Address, list.Address)
      errs = append(errs, errors.New("mail address is not allowed to post"))
      handleErrorResponse(from, msg)
      continue
    }

    sendErrs := list.SendInBehaveOf(from, msg)
    if len(errs) > 0 {
      errs = append(errs, sendErrs...)
    }
  }

  // XXX send an error to postmaster
  for _, err := range errs {
    logger.Error().Err(err).Msg("unable to send mail")
  }

  logger.Info().Msg("message handled")
}

func handleErrorResponse(to *mail.Address, msg *mail.Message) {
  newMsg := mail.Message{Header: make(mail.Header)}

  from := fmt.Sprintf(config.PrefixAddress, "no-reply")
  body := fmt.Sprintf("Dear %s,\n\nyour message to %s was not processed!\nIf you think that is a mistake contact %s for help.\n\nKind regards,\nAdmin-Team",
    to.Address, msg.Header.Get("To"), config.PostmasterAddress)

  newMsg.Header["To"] = []string{to.Address}
  newMsg.Header["From"] = []string{from}
  newMsg.Header["In-Reply-To"] = headerValue("Id", msg.Header)

  subject := strings.Builder{}
  subject.WriteString("Re: ")
  subject.WriteString(msg.Header.Get("Subject"))
  newMsg.Header["Subject"] = []string{subject.String()}
  newMsg.Header["Content-Type"] = []string{"text/plain"}

  if !config.Debug {
    auth := smtp.PlainAuth(
      "", config.SMTPUsername, config.SMTPPassword, config.SMTPHostname)
    err := smtp.SendMail(fmt.Sprintf("%s:%d", config.SMTPHostname, config.SMTPPort),
      auth, from, []string{to.Address}, toBytes(&newMsg, body))
    if err != nil {
      logger.Error().Err(err).Msg("cannot send error response")
      return
    }
  }
  logger.Debug().Msgf("sent error mail to %s", to.Address)
}

// find all mailing lists mentioned in the message
func lookupLists(msg *mail.Message) (lists []*List) {
  if toList, err := mail.ParseAddressList(msg.Header.Get("To")); err == nil {
    for _, to := range toList {
      list := lookupList(to.Address)
      if list != nil {
        lists = append(lists, list)
      }
    }
  }

  if ccList, err := mail.ParseAddressList(msg.Header.Get("Cc")); err == nil {
    for _, cc := range ccList {
      list := lookupList(cc.Address)
      if list != nil {
        lists = append(lists, list)
      }
    }
  }

  if bccList, err := mail.ParseAddressList(msg.Header.Get("Bcc")); err == nil {
    for _, bcc := range bccList {
      list := lookupList(bcc.Address)
      if list != nil {
        lists = append(lists, list)
      }
    }
  }
  return lists
}

// check if the mail address is a known mailing list
func lookupList(addressLine string) *List {
  address, err := mail.ParseAddress(addressLine)
  if err != nil {
    logger.Debug().Err(err).Msgf("cannot parse address line: %s", addressLine)
    return nil
  }

  for _, list := range ldapLists {
    if address.Address == list.Address {
      return &list
    }
  }
  return nil
}

// check if FROM is subscribed to a mailing list
func isSubscribed(from string, list string) bool {
  obj, err := mail.ParseAddress(from)
  if err != nil {
    logger.Fatal().Err(err).Msgf("cannot parse mail address: %s", from)
  }

  res := fetchAttribute(fmt.Sprintf("(&(mailalias=%s)(|(cn=%s)(mail=%s)))",
    obj.Address, list, list), "mailalias")
  for _, alias := range res {
    if alias == obj.Address {
      return true
    }
  }
  return false
}

func toBytes(msg *mail.Message, body string) []byte {
  var newMsg bytes.Buffer
  for key, list := range msg.Header {
    for _, val := range list {
      fmt.Fprintf(&newMsg, "%s: %s\r\n", key, val)
    }
  }
  fmt.Fprintf(&newMsg, "\r\n%s\r\n", body)
  return newMsg.Bytes()
}

func headerValue(key string, dataMap map[string][]string) []string {
  if value, ok := dataMap[key]; ok {
    return value
  }
  return []string{}
}
