package main
//
// Microlist - A simple mailinglist for postfix with ldap backend
// Copyright (C) 2020  Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
import (
  "bytes"
  "net/mail"
  "net/smtp"
  "fmt"
)

type List struct {
  Id string
  Name string
  Description string
  Address string
  Hidden bool
  SubscribersOnly bool
}

func (list *List) CanPost(from *mail.Address) bool {
  // check if the list is restricted to subscribers only
  // and whether FROM is part of it or not
  if list.SubscribersOnly && !isSubscribed(from.Address, list.Id) {
    return false
  }
  return true
}

func (list *List) SendInBehaveOf(from *mail.Address, received *mail.Message) (errs []error) {
  newMsg := mail.Message{Header: make(mail.Header)}
  newMsg.Header["To"] = []string{list.Address}
  if list.SubscribersOnly {
    newMsg.Header["Reply-To"] = []string{list.Address}
  } else {
    newMsg.Header["Reply-To"] = []string{
      fmt.Sprintf("%s,%s", list.Address, from.Address),
    }
  }
  newMsg.Header["In-Reply-To"] = headerValue("Id", received.Header)
  newMsg.Header["Content-Type"] = headerValue("Content-Type", received.Header)
  newMsg.Header["Content-Transfer-Encoding"] = headerValue("Content-Transfer-Encoding", received.Header)

  newMsg.Header["From"] = []string{list.Address}
  newMsg.Header["List-ID"] = []string{list.Address}
  newMsg.Header["Subject"] = headerValue("Subject", received.Header)

  recipients := fetchSubscribers(list.Id)

  buffer := new(bytes.Buffer)
  buffer.ReadFrom(received.Body)

  if !config.Debug {
    auth := smtp.PlainAuth(
      "", config.SMTPUsername, config.SMTPPassword, config.SMTPHostname)
    err := smtp.SendMail(fmt.Sprintf("%s:%d", config.SMTPHostname, config.SMTPPort),
      auth, list.Address, recipients, toBytes(&newMsg, buffer.String()))
    if err != nil {
      errs = append(errs, err)
    }
  }

  logger.Debug().
    Strs("recipients", recipients).
    Str("body", buffer.String()).
    Msg("mail was forwarded")

  return
}
