package main
//
// Microlist - A simple mailinglist for postfix with ldap backend
// Copyright (C) 2020  Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
import (
  "flag"
  "bufio"
  "os"
  "strings"
  "net/mail"
  "fmt"

  "gopkg.in/ini.v1"
  "github.com/rs/zerolog"
)

var debug bool
var config Config
var configFile string
var ldapLists map[string]List
var logger = zerolog.New(os.Stdout).
  Output(zerolog.ConsoleWriter{Out: os.Stdout}).
  With().
  Stack().
  Caller().
  Timestamp().
  Logger()

func init() {
  flag.BoolVar(&debug, "debug", false,
    "Do not send anything - Mails will be printed to stdout")
  flag.StringVar(&configFile, "config", "config.ini",
    "Load configuration from specified file")
  flag.Parse()

  cfg, err := ini.Load(configFile)
  if err != nil {
    logger.Fatal().Err(err).Msg("cannot load config")
  }

  err = cfg.Section("").MapTo(&config)
  if err != nil {
    logger.Fatal().Err(err).Msg("cannot map config to struct")
  }
}

func main() {
  if len(flag.Args()) < 1 {
    logger.Fatal().Msg("no command specified")
  }

  // set log level to info if debug is not set
  if !config.Debug {
    zerolog.SetGlobalLevel(zerolog.InfoLevel)
  }

  // replace stdout with log file
  if config.LogFile != "" {
    logFile, err := os.OpenFile(config.LogFile,
      os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
    if err != nil {
      logger.Fatal().Err(err).Msg("cannot open file")
    }
    defer logFile.Close()

    logger = logger.Output(logFile)
  }

  // load ldap mailing lists
  ldapLists = make(map[string]List)
  results := fetchAttributes("(&(objectClass=mailList))",
    []string{"cn", "subscribersonly", "title", "description"})

  for _, result := range results {
    list := List{}
    for _, attr := range result.Attributes {
      if strings.EqualFold(attr.Name, "cn") {
        list.Id = strings.Join(attr.Values, "")
        list.Address = fmt.Sprintf(config.PrefixAddress, list.Id)
      }
      if strings.EqualFold(attr.Name, "subscribersonly") {
        list.SubscribersOnly = strings.EqualFold("true",
          strings.Join(attr.Values, ""))
      }
      if strings.EqualFold(attr.Name, "title") {
        list.Name = strings.Join(attr.Values, "")
      }
      if strings.EqualFold(attr.Name, "description") {
        list.Description = strings.Join(attr.Values, "")
      }
    }
    ldapLists[list.Address] = list
  }

  logger.Info().Msg("starting microlist")
  if flag.Arg(0) == "message" {
    receivedMessage, err := mail.ReadMessage(bufio.NewReader(os.Stdin))
    if err != nil {
      logger.Fatal().Err(err).Msg("cannot parse receiving message")
    }

    logger.Info().
      Str("id", receivedMessage.Header.Get("Id")).
      Str("from", receivedMessage.Header.Get("From")).
      Str("to", receivedMessage.Header.Get("To")).
      Str("cc", receivedMessage.Header.Get("Cc")).
      Str("bcc", receivedMessage.Header.Get("Bcc")).
      Str("subject", receivedMessage.Header.Get("Subject")).
      Str("content-type", receivedMessage.Header.Get("Content-Type")).
      Msg("message received")

    handleReceivedMessage(receivedMessage)
  } else {
    logger.Error().Msgf("unkown command: %s", flag.Arg(0))
  }
}
