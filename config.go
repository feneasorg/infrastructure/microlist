package main
//
// Microlist - A simple mailinglist for postfix with ldap backend
// Copyright (C) 2020  Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

type Config struct {
  PostmasterAddress string `ini:"postmaster_address"`
  PrefixAddress string `ini:"prefix_address"`
  LogFile string `ini:"log_file"`
  LDAPHost string `ini:"ldap_host"`
  LDAPBase string `ini:"ldap_base"`
  LDAPBindDN string `ini:"ldap_bind_dn"`
  LDAPBindPW string `ini:"ldap_bind_pw"`
  SMTPHostname string `ini:"smtp_hostname"`
  SMTPPort int `ini:"smtp_port"`
  SMTPUsername string `ini:"smtp_username"`
  SMTPPassword string `ini:"smtp_password"`
  Debug bool `ini:"debug"`
}
